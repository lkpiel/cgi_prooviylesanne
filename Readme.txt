Kokku v�ttis �lesande lahendamine aega 13h.

K�ige kauem v�ttis aega kuup�eva ja kellaaja valiku lisamine kasutajaliidesele. Proovisin mitmeid erinevaid liideseid, kuid �kski
ei toiminud soovitud viisil. Implementeeritud lahenduse puuduj��giks on segane kellaaja valik, sest lisatud liides v�imaldab valida
vaid t�istunde. Muu kellaaja valimiseks tuleb kellaaega vastavas lahtris muuta manuaalselt. 

Lisasin ka DentistVisitService klassi Unit testid.

Kommentaarid lahendatud �lesannete kohta:
1) 
Kasutajaliidese visuaalse poole parandamiseks kasutasin Bootstrapi.
Kuna esialgne serveripoolne valideerimine oli juba implementeeritud, lisasin, et saadetav kuup�ev peab sisaldama 
ka kellaaega formaadis HH:mm. Lisasin ka valideeringu, et kuup�ev peab olema tulevikus.
Getterite, setterite ja konstruktoirte tegemiseks kasutasin lombokit. Eemaldasin entity objektilt setterid ja asendasin need 
informatiivsete nimedega meetoditega. 
DTO objekti muutmiseks entityks ja entity muutmiseks DTO objektis l�in DentistVisitAssembler klassi. 
Et mitte korduvalt navigeerimisriba ja headerit luua, l�in nende kohta thymleafi poolt taaskasutatavad fragmendid.

2) 
Registreeringud kuvatakse ajalises j�rjestuses alates ajaliselt k�ige varasemast registreeringust.

3) 
Registreeringute otsing on lahendadud front-end funkstionaalsusega kasutades javascripti. V�imekama otsingu tegemiseks
oleks v�inud teha seda ka back-endis.

4) Registreeringu mitte leidmisel visatakse exception VisitNotFoundException
  4.1) Registreeringute muutmisel valideeritakse muudetavad v�ljad uuesti.
  4.3) Kattuvate visiitide m��ramiseks otsustasin, et �he visiidi pikkus on 15 minutit. Kui visiidi loomine ei �nnestu visatakse
       FailedToCreateVisitException

Rakendus k�ib aadressil localhost:8080
