package com.cgi.dentistapp.dao;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import com.cgi.dentistapp.common.FailedToCreateVisitException;
import com.cgi.dentistapp.common.VisitNotFoundException;
import org.springframework.stereotype.Repository;

import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

import java.text.SimpleDateFormat;
import java.util.List;

@Repository
public class DentistVisitDao {

    @PersistenceContext
    private EntityManager entityManager;
    static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs

    public DentistVisitEntity create(DentistVisitEntity visit) throws FailedToCreateVisitException {
        SimpleDateFormat sm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String visitStart = sm.format(visit.getVisitDate());
        String visitEnd = sm.format(visit.getVisitDate().getTime() + 15*ONE_MINUTE_IN_MILLIS);
        String fifteenMinutesBeforeVisit = sm.format(visit.getVisitDate().getTime() - 15*ONE_MINUTE_IN_MILLIS);
        List<DentistVisitEntity> dentistVisitEntities = entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE dentist_name = '"+visit.getDentistName()+ "' AND visit_date > '" +fifteenMinutesBeforeVisit +"' AND visit_date < '" + visitEnd + "'").getResultList();
        if(dentistVisitEntities.size() > 0){
            throw new FailedToCreateVisitException();
        } else {
            entityManager.persist(visit);
            return visit;
        }

    }

    public List<DentistVisitEntity> getAllVisits() {
        return entityManager.createQuery("SELECT e FROM DentistVisitEntity e ORDER BY visit_date ASC").getResultList();
    }

    public DentistVisitEntity getVisit(Long id) throws VisitNotFoundException {
        try {
            DentistVisitEntity dentistVisitEntity = (DentistVisitEntity) entityManager.createQuery("SELECT e FROM DentistVisitEntity e WHERE e.id = " + id + "").getSingleResult();
            return dentistVisitEntity;

        } catch (NoResultException e){
            throw new VisitNotFoundException(id.toString());
        }
    }

    public DentistVisitEntity updateVisit(DentistVisitEntity dentistVisitEntity) {
        entityManager.merge(dentistVisitEntity);
        return dentistVisitEntity;
    }

    public void deleteVisit(DentistVisitEntity dentistVisitEntity) {
        Object managed = entityManager.merge(dentistVisitEntity);
        entityManager.remove(managed);
    }
}
