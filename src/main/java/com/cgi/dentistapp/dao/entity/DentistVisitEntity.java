package com.cgi.dentistapp.dao.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@NoArgsConstructor(force = true)
@AllArgsConstructor(staticName = "of")
@Table(name = "dentist_visit")
public class DentistVisitEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String dentistName;

    @Column(name = "visit_date")
    @DateTimeFormat(pattern = "dd.MM.yyyy HH:mm")
    private Date visitDate;


    public static DentistVisitEntity of(String dentistName, Date visitDate){
        DentistVisitEntity dentistVisitEntity = new DentistVisitEntity();
        dentistVisitEntity.updateDentistName(dentistName);
        dentistVisitEntity.updateVisitDate(visitDate);
        return dentistVisitEntity;
    }
    public void updateDentistName(String dentistName){this.dentistName = dentistName;}
    public void updateVisitDate(Date date){this.visitDate = date;}


}
