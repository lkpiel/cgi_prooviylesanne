package com.cgi.dentistapp.common;

/**
 * Created by lkpiel on 4/17/2017.
 */
public class FailedToCreateVisitException  extends Exception {
    private static final long serialVersionUID = 1L;
    public FailedToCreateVisitException() {
        super(String.format("Unable to create visit, because times overlap"));
    }
}