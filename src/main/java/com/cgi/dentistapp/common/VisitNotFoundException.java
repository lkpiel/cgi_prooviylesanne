package com.cgi.dentistapp.common;

/**
 * Created by lkpiel on 4/17/2017.
 */
public class VisitNotFoundException  extends Exception {
    private static final long serialVersionUID = 1L;
    public VisitNotFoundException(String id) {
        super(String.format("Visit not found! (Visit id: %s)", id));
    }
}