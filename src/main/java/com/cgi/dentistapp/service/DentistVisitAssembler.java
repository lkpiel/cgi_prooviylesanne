package com.cgi.dentistapp.service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


/**
 * Created by lkpiel on 4/12/2017.
 */
@Service
public class DentistVisitAssembler  {
    @Autowired
    DentistVisitDao dentistVisitDao;
    public DentistVisitDTO toResource(DentistVisitEntity dentistVisitEntity) {
        return DentistVisitDTO.of(dentistVisitEntity.getId(), dentistVisitEntity.getDentistName(), dentistVisitEntity.getVisitDate());
    }
    public DentistVisitEntity toEntity(DentistVisitDTO dentistVisitDTO) {
        return DentistVisitEntity.of(dentistVisitDTO.getId(), dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitDate());
    }
}
