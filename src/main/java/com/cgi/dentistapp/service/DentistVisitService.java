package com.cgi.dentistapp.service;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import com.cgi.dentistapp.common.FailedToCreateVisitException;
import com.cgi.dentistapp.common.VisitNotFoundException;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cgi.dentistapp.dao.DentistVisitDao;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;

@Service
@Transactional
public class DentistVisitService {

    @Autowired
    private DentistVisitDao dentistVisitDao;

    @Autowired
    private DentistVisitAssembler dentistVisitAssembler;

    public DentistVisitDTO addVisit(String dentistName, Date visitDate) {
        DentistVisitEntity visit = DentistVisitEntity.of(dentistName,visitDate);
        try{
            DentistVisitEntity createdVisit = dentistVisitDao.create(visit);
            return dentistVisitAssembler.toResource(createdVisit);
        } catch (FailedToCreateVisitException e) {
            System.err.println("FailedToCreateVisitException: " + e.getMessage());
            return null;
        }
    }

    public List<DentistVisitDTO> listVisits () {
        List<DentistVisitEntity> dentistVisitEntities = dentistVisitDao.getAllVisits();
        List<DentistVisitDTO> dentistVisitEntityDTOs = new ArrayList<>();
        for (DentistVisitEntity dentistVisitEntity : dentistVisitEntities){
            dentistVisitEntityDTOs.add(dentistVisitAssembler.toResource(dentistVisitEntity));
        }
        return dentistVisitEntityDTOs;
    }

    public DentistVisitDTO getVisit(Long id) {
        DentistVisitEntity dentistVisitEntity = null;
        try {
            dentistVisitEntity = dentistVisitDao.getVisit(id);
            return dentistVisitAssembler.toResource(dentistVisitEntity);
        } catch (VisitNotFoundException e) {
            System.err.println("VisitNotFoundException: " + e.getMessage());
            return null;
        }
    }

    public DentistVisitDTO updateVisit(DentistVisitDTO dentistVisitDTO) {
        DentistVisitEntity dentistVisitEntity = dentistVisitAssembler.toEntity(dentistVisitDTO);
        DentistVisitEntity updatedDentistVisitEntity = dentistVisitDao.updateVisit(dentistVisitEntity);
        return dentistVisitAssembler.toResource(updatedDentistVisitEntity);
    }

    public void deleteVisit(DentistVisitDTO dentistVisitDTO) {
        DentistVisitEntity dentistVisitEntity = dentistVisitAssembler.toEntity(dentistVisitDTO);
        dentistVisitDao.deleteVisit(dentistVisitEntity);
    }
}
