package com.cgi.dentistapp.controller;

import com.cgi.dentistapp.common.VisitNotFoundException;
import com.cgi.dentistapp.dao.entity.DentistVisitEntity;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import com.cgi.dentistapp.service.DentistVisitService;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

@Controller
@EnableAutoConfiguration
public class DentistAppController extends WebMvcConfigurerAdapter {

    @Autowired
    private DentistVisitService dentistVisitService;

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/results").setViewName("results");
    }

    @GetMapping("/")
    public String showRegisterForm(DentistVisitDTO dentistVisitDTO) {
        return "form";
    }

    @PostMapping("")
    public String postRegisterForm(@Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) {
        Date date = new Date();

        if (bindingResult.hasErrors() || dentistVisitDTO.getVisitDate().before(date)) {
            if (dentistVisitDTO.getVisitDate() != null && dentistVisitDTO.getVisitDate().before(date)) {
                bindingResult.rejectValue("visitDate", "dateNotAfterToday");
            }
            return "form";
        }
        DentistVisitDTO addedDentistVisitDTO = dentistVisitService.addVisit(dentistVisitDTO.getDentistName(), dentistVisitDTO.getVisitDate());
        if (addedDentistVisitDTO != null) {
            model.addAttribute("info", "Visiit edukalt registreeritud!");
        } else {
            model.addAttribute("error", "See aeg on juba võetud. Palun vali muu aeg!");
        }
        return "form";
    }

    @GetMapping("/visits")
    public String getRegisteredVisits(@RequestParam(value = "info", required = false) String info, Model model) throws ParseException {
        if (info != null) {
            model.addAttribute("info", info);

        }
        model.addAttribute("visits", dentistVisitService.listVisits());
        return "visits";
    }

    @GetMapping("/visits/{id}")
    public String getVisit(@PathVariable Long id, Model model) throws ParseException {
        DentistVisitDTO dentistVisitDTO = dentistVisitService.getVisit(id);
        if (dentistVisitDTO == null) {
            model.addAttribute("error", "Visiiti ID-ga " + id + "ei leitud!");
            return "error";
        }
        model.addAttribute("dentistVisitDTO", dentistVisitDTO);
        return "visit";
    }

    @PostMapping("/visits/{id}/update")
    public String updateVisit(@PathVariable Long id, @Valid DentistVisitDTO dentistVisitDTO, BindingResult bindingResult, Model model) throws ParseException {
        Date date = new Date();
        if (bindingResult.hasErrors() || dentistVisitDTO.getVisitDate().before(date)) {
            if ( dentistVisitDTO.getVisitDate() != null && dentistVisitDTO.getVisitDate().before(date)) {
                bindingResult.rejectValue("visitDate", "dateNotAfterToday");
            }
            model.addAttribute("dentistVisitDTO", dentistVisitDTO);
            return "visit";
        }
        DentistVisitDTO updatedDentistVisitDTO = dentistVisitService.updateVisit(dentistVisitDTO);
        model.addAttribute("dentistVisitDTO", updatedDentistVisitDTO);
        model.addAttribute("info", "Visiit on uudendatud!");
        return "visit";
    }

    @PostMapping("/visits/{id}/delete")
    public String removeVisit(@PathVariable Long id, DentistVisitDTO dentistVisitDTO, Model model, RedirectAttributes redirectAttributes) throws ParseException {
        dentistVisitService.deleteVisit(dentistVisitDTO);
        redirectAttributes.addAttribute("info", "Visiit nr " + id + " on kustutatud!");
        return "redirect:/visits";
    }
}
