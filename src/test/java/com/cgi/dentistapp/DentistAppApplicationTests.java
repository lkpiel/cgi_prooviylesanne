package com.cgi.dentistapp;

import com.cgi.dentistapp.common.FailedToCreateVisitException;
import com.cgi.dentistapp.dto.DentistVisitDTO;
import com.cgi.dentistapp.service.DentistVisitService;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest(classes= DentistAppApplication.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class DentistAppApplicationTests {
	@Autowired
	DentistVisitService dentistVisitService;

	@Rule
	public ExpectedException thrown = ExpectedException.none();

	@Test
	public void addVisitTest() {
		// find one info element
		Calendar randomDate = Calendar.getInstance();
		DentistVisitDTO createdDentistVisitDTO = dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		//1. Kontrollib, et meetod ei leiaks planti, kui periood on vähem kui kolm nädalat praegusest, kuigi talle on vahepeal maintenance määratud. Loe ülesande juhendi alt Relaxed punkti.
		assertThat(createdDentistVisitDTO).isNotNull();
		assertThat(createdDentistVisitDTO.getDentistName().equalsIgnoreCase("Toomas Kaun"));
		assertThat(createdDentistVisitDTO.getVisitDate().equals(randomDate));


	}
	@Test
	public void addDupilcateVisitTest(){
		Calendar randomDate = Calendar.getInstance();
		randomDate.set(2017, 12, 12, 12, 00, 00);
		dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		DentistVisitDTO duplicateDentistVisitDTO = dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		assertThat(duplicateDentistVisitDTO).isNull();
	}
	@Test
	public void listVisitsTest() {
		List<DentistVisitDTO> dentistVisitDTOList = dentistVisitService.listVisits();
		Calendar randomDate = Calendar.getInstance();
		randomDate.set(2017, 12, 12, 12, 00, 00);
		Calendar anotherDate = Calendar.getInstance();
		anotherDate.set(2017, 12, 12, 12, 00, 00);
		assertThat(dentistVisitDTOList.size()).isEqualTo(0);
		DentistVisitDTO dentistVisitDTO = dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		DentistVisitDTO anotherDentistVisitDTO = dentistVisitService.addVisit("Kalle Kand", anotherDate.getTime());
		dentistVisitDTOList = dentistVisitService.listVisits();
		assertThat(dentistVisitDTOList.size()).isEqualTo(2);
		assertThat(dentistVisitDTOList.contains(dentistVisitDTO));
		assertThat(dentistVisitDTOList.contains(anotherDentistVisitDTO));
	}
	@Test
	public void getVisitTest(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm");
		Calendar randomDate = Calendar.getInstance();

		assertThat(dentistVisitService.getVisit(1l)).isNull();
		dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		assertThat(dentistVisitService.getVisit(1l).getDentistName()).isEqualToIgnoringCase("Toomas Kaun");
		assertThat(simpleDateFormat.format(dentistVisitService.getVisit(1l).getVisitDate())).isEqualToIgnoringCase(simpleDateFormat.format(randomDate.getTime()));
	}
	@Test
	public void updateVisitTest(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm");
		Calendar randomDate = Calendar.getInstance();
		DentistVisitDTO createdDentistVisitDTO = dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		createdDentistVisitDTO.setDentistName("Sirje Kaup");
		DentistVisitDTO updatedDentistVisitDTO = dentistVisitService.updateVisit(createdDentistVisitDTO);

		assertThat(updatedDentistVisitDTO.getId()).isEqualTo(createdDentistVisitDTO.getId());
		assertThat(updatedDentistVisitDTO.getDentistName()).isEqualToIgnoringCase("Sirje Kaup");
		assertThat(simpleDateFormat.format(updatedDentistVisitDTO.getVisitDate())).isEqualToIgnoringCase(simpleDateFormat.format(createdDentistVisitDTO.getVisitDate()));
	}

	@Test
	public void deleteVisitTest(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.YYYY HH:mm");
		Calendar randomDate = Calendar.getInstance();
		DentistVisitDTO createdDentistVisitDTO = dentistVisitService.addVisit("Toomas Kaun", randomDate.getTime());
		assertThat(dentistVisitService.getVisit(1l)).isNotNull();

		dentistVisitService.deleteVisit(createdDentistVisitDTO);

		assertThat(dentistVisitService.getVisit(1l)).isNull();

	}

}
